class Event < ActiveRecord::Base
  attr_accessible :additionallistingtext, :dates, :description, :eventimages, :eventname, :eventurl, :facebookeventid, :prices, :status, :tags, :videoembed
  serialize :dates
  serialize :eventimages
  serialize :prices
  has_one :venue
  has_many :attractions
end
