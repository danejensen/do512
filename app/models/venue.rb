class Venue < ActiveRecord::Base
  attr_accessible :address, :city, :country, :name, :postalcode, :state, :twitterid, :venueimages, :venueurl, :venueid
  serialize :venueimages, Hash
end
