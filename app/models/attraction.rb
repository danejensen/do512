class Attraction < ActiveRecord::Base
  attr_accessible :artist, :artistid, :billing, :event_id, :genre, :links, :sequence, :video, :image
end
