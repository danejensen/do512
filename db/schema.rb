# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120611035815) do

  create_table "attractions", :force => true do |t|
    t.string   "sequence"
    t.string   "artist"
    t.string   "artistid"
    t.string   "billing"
    t.string   "genre"
    t.string   "links"
    t.integer  "event_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "video"
    t.string   "image"
  end

  create_table "events", :force => true do |t|
    t.string   "facebookeventid"
    t.string   "eventname"
    t.text     "description"
    t.string   "eventurl"
    t.string   "additionallistingtext"
    t.string   "status"
    t.string   "tags"
    t.string   "videoembed"
    t.text     "dates"
    t.text     "eventimages"
    t.text     "prices"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "venues", :force => true do |t|
    t.string   "name"
    t.string   "venueurl"
    t.string   "city"
    t.string   "state"
    t.string   "postalcode"
    t.string   "country"
    t.string   "address"
    t.string   "twitterid"
    t.text     "venueimages"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "event_id"
    t.string   "venueid"
  end

end
