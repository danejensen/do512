class AddImageToAttractions < ActiveRecord::Migration
  def change
    add_column :attractions, :image, :string
  end
end
