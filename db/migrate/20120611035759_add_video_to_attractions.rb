class AddVideoToAttractions < ActiveRecord::Migration
  def change
    add_column :attractions, :video, :string
  end
end
