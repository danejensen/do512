class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :facebookeventid
      t.string :eventname
      t.text :description
      t.string :eventurl
      t.string :additionallistingtext
      t.string :status
      t.string :tags
      t.string :videoembed
      t.text :dates
      t.text :eventimages
      t.text :prices

      t.timestamps
    end
  end
end
