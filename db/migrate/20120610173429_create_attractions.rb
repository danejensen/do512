class CreateAttractions < ActiveRecord::Migration
  def change
    create_table :attractions do |t|
      t.string :sequence
      t.string :artist
      t.string :artistid
      t.string :billing
      t.string :genre
      t.string :links
      t.integer :event_id

      t.timestamps
    end
  end
end
