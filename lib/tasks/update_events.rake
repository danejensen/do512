require 'uri'
require 'net/http'
require 'json'
require 'cgi'

namespace :events do
	task :update, [:url] => :environment do |t, args|
		uri = URI.parse(args["url"])
		response = Net::HTTP.get_response(uri)
		j = JSON.parse(response.body)
		#vid = event["venue"].delete("venueid")

		while j["currentPage"] != "0" do
			j["events"].each do |event|
				v = Venue.find_by_venueid(event["venue"]["venueid"])
				if v
					v.update_attributes(event["venue"])
				else 
					v = Venue.create(event["venue"])
					v.event_id = event["eventid"]
					v.save
				end
				event.delete("venue")

				event["attractionList"].each do |attract|
					a = Attraction.find_by_artistid(attract["artistid"])
					if a
						a.update_attributes(attract)
					else 
						a = Attraction.create(attract)
						a.event_id = event["eventid"]
						a.save
					end		
				end

				event.delete("attractionList")
				eid = event.delete("eventid")
				e = Event.find_by_id(eid)
				if e
					e.update_attributes(event)
				else
					e = Event.create(event)
					sql = "update events set id=%s where id=%d" % [eid, e.id]
					ActiveRecord::Base.connection.execute(sql)
				end

			end

			qs = CGI.parse(uri.query)
			page = Integer(qs["page"][0])
			page = page + 1
			url = "http://api.ticketweb.com/snl/EventAPI.action?key=OnTLfy5CJ7XX1mLwynRp&version=1&method=json&page=" + String(page)
			uri = URI.parse(url)
			response = Net::HTTP.get_response(uri)
			j = JSON.parse(response.body)
			sleep(5)
			puts page
		end
	end
end